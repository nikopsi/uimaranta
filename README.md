# uimaranta

Hakee uimarannat Helsingin kaupungin APIsta https://iot.fvh.fi/opendata/uiras/ ja tulostaa sen uimarannan,
jossa viimeisimmän mittauksen mukaan on lämpimin vesi. Rannat, joiden viimeisin mittaus on yli kolme tuntia vanha, ohitetaan.

## Usage

### Uberjar

Käännä projekin uberjar `lein uberjar`. Suorita käännetty jar `java -jar target/uimaranta-0.1.0-SNAPSHOT-standalone.jar `

### REPL
Käynnistä REPL projektin juuressa `lein repl` ja suorita funktio `(-main)`
