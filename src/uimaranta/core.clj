(ns uimaranta.core
  (:gen-class)
  (:require [uimaranta.beach :as beach])
  (:import (java.time.temporal ChronoUnit)
           (java.time ZonedDateTime)))

(def print-format
  "Ranta, jossa lämpimin vesi
Nimi: %s
Palvelukartta: %s
Veden lämpötila: %.2f °C
Mitattu %d minuuttia sitten")

(defn- get-first-valid-temp [beach-data]
   (first (drop-while #(some nil? ((juxt :time :temp_water) %)) (:data beach-data))))

(defn- get-minutes-since [d]
  (.between ChronoUnit/MINUTES d (ZonedDateTime/now)))


(defn get-beach-with-highest-water-temp
  []
  (->> (beach/get-each-beach-data)
       (map #(assoc % :latest-valid-temp (get-first-valid-temp %)))
       (filter #(not (nil? (:latest-valid-temp %))))
       (map #(assoc % :minutes-since-latest-valid-temp (get-minutes-since (get-in % [:latest-valid-temp :time]))))
       (filter #(<= (:minutes-since-latest-valid-temp %) 180))
       (sort (fn [x y] (apply compare (map #(get-in % [:latest-valid-temp :temp_water]) [y x]))))
       first))

(defn -main []
  (let [{:keys [minutes-since-latest-valid-temp]
         {:keys [name servicemap_url]} :meta
         {:keys [temp_water]} :latest-valid-temp
         } (get-beach-with-highest-water-temp)]
    (println (format print-format name servicemap_url temp_water minutes-since-latest-valid-temp))))

