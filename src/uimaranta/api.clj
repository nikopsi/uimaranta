(ns uimaranta.api
  (:require [clj-http.client :as client]))


(defn- get-body-json [url]
  (:body (client/get url {:accept :application/json :as :json})))

(defn- get-beaches-metadata []
  (get-body-json "https://iot.fvh.fi/opendata/uiras/uiras-meta.json"))

(defn get-beach-ids []
  (keys (get-beaches-metadata)))

(defn get-beach-by-id [id]
  {:pre [(keyword? id)]}
  (get-body-json (format "https://iot.fvh.fi/opendata/uiras/%s_v1.json" (name id))))
