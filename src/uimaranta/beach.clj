(ns uimaranta.beach
  (:require [uimaranta.api :as api])
  (:import (java.time ZonedDateTime DateTimeException)
           (java.time.format DateTimeFormatter)))

(defn- date-comparator [x y]
  (- (.toEpochSecond (:time y)) (.toEpochSecond (:time x))))


(defn- parse-timestamp [data-entry]
  (try
    (update data-entry :time #(ZonedDateTime/parse % DateTimeFormatter/ISO_OFFSET_DATE_TIME))
    (catch DateTimeException _ nil)))

(defn- parse-data-entry-timestamps [beach]
  (update beach :data #(map parse-timestamp %)))

(defn- order-data-entries-by-time [beach]
  (update beach :data #(sort date-comparator %)))

(defn- parse-beach-data [beach-json]
  (->
    beach-json
    parse-data-entry-timestamps
    order-data-entries-by-time))

(defn get-each-beach-data []
  (->> (api/get-beach-ids)
       (pmap api/get-beach-by-id)
       (map parse-beach-data)))